/* ---------------------------------------
 Exported Module Variable: LinkParam
 Package:  linkparam
 Version:  1.2.1
 Homepage: https://github.com/niebert/LinkParam#readme
 Author:   Engelbert Niehaus
 License:  MIT
 Date:     2020/11/16 3:16:17
 Require Module with:
    const LinkParam = require('linkparam');
 ------------------------------------------ */

//--- JSHint Settings: -----
/*jshint  laxcomma: true, asi: true, maxerr: 150 */
/*global alert, confirm, console, prompt */
