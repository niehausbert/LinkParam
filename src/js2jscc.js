/*
--- js2jscc.js ---
js2jscc.js requires javascript file and
creates a JavaScript Class as Constructor in UML Unified Modelling Language
SOURCE: src/main.js  as constructor
*/
var pkg = require("../package.json");
// Input UML file is used to extract predefined comments for attributes and methods
var vInputUML = pkg.name+"_uml.json";
var vOutputUML = pkg.name+"_uml_build.json";
var fs = require('fs');
var vConstructor = require('../src/main.js');
var vUML = require("../jscc/"+vInputUML);
var vPrototype = vConstructor.prototype;

function update_uml(pUML,pkg) {
  if (pkg.hasOwnProperty("epxortvar")) {
    pUML.data.classname = pkg.exportvar;
  } else  {
    console.log("package.json has no property 'exportvar' e.g. ");
  }
  // Package description as Comment for class
  pUML.data.comment = pkg.description;
  // Time and Date
  if (pUML.data.reposinfo && pUML.data.reposinfo.modified) {
    pUML.data.reposinfo.modified = getDateTime();
  }
  // update URL to repository
  var vRepo = pkg.repository.url;
	var vBegin = vRepo.indexOf("https:");
	var vEnd = vRepo.lastIndexOf(".git");
	var vURL = pkg.repository.url;
	if ((vBegin >= 0) && (vEnd > vBegin)) {
		vURL = "https:" + vRepo.substring(vBegin,vEnd);
	};

}


function outTime(pNr) {
	var vOut = pNr;
	if (pNr == 0) {
		vOut = "00"
	} if (pNr<10) {
		vOut = "0"+pNr;
	};
	return vOut
}

function getDateTime() {
	var vNow = new Date();
	var vSep = "/"; // set separator for date
	var vOut = vNow.getFullYear() + vSep +outTime(vNow.getMonth()+1) + vSep + outTime(vNow.getDate());
  vOut += " "; // Separator between Date and Time
	vSep = ":"; // set separator for time
	vOut += vNow.getHours() + vSep + outTime(vNow.getMinutes()) + vSep + outTime(vNow.getSeconds());
	return vOut;
}


function cloneJSON(pJSON) {
    return JSON.parse(JSON.stringify(pJSON));
}

function extract_body(pFunctionDef) {
  var vString = pFunctionDef + " ";
  var vBody = "";
  if (vString) {
    var begin = vString.indexOf("{");
    var end   = vString.lastIndexOf("}");
    vBody = vString.substring(begin+1,end);
  }
  return vBody;
}

function extract_param(pFunctionDef) {
  var vString = pFunctionDef + " ";
  var vParam = "";
  if (vString) {
    var begin = vString.indexOf("(");
    var end   = vString.indexOf(")");
    vParam = vString.substring(begin+1,end);
  }
  return vParam;
}

function find_name_index(pname,parray) {
  var vFound = -1;
  for (var i = 0; i < parray.length; i++) {
    if (pname == parray[i].name) {
      vFound = i;
    }
  }
  return vFound;
}

function get_method_parameter(pFunctionDef,pmethod) {
  var vParamString = extract_param(pFunctionDef);
  var vParArr = vParamString.split(",");
  var vNewPar = [];
  // console.log("pmethod="+JSON.stringify(pmethod,null,4));
  var vOldPar = pmethod.parameter;
  for (var i = 0; i < vParArr.length; i++) {
    if (vParArr[i] !== "") {
      var par_i = find_name_index(vParArr[i],vOldPar);
      if (par_i >= 0) {
        // paraemeter already exists in UML of method
        // push old parameter definition to new parameter array
        vNewPar.push(cloneJSON(vOldPar[par_i]));
      } else {
        // create the new parameter in UML model
        vNewPar.push({
            "name": vParArr[i],
            "class": " ",
            "comment": "the parameter provides ..."
          });
      }
    }
  }
  //pmethod.parameter = vNewPar;
  return vNewPar;
}


function get_method(pMethodID,pFunctionDef,pData) {
  var vMethodHash = null;
  var vParamString = extract_param(pFunctionDef);
  var meth_i = find_name_index(pMethodID,pData.methods);
  if (meth_i >= 0) {
    console.log("Method '" + pMethodID + "(" + vParamString + ")' found");
    // method name found in vUML.data.methods
    // update the code
    vMethodHash = cloneJSON(pData.methods[meth_i]);
    vMethodHash.code = extract_body(pFunctionDef);
    vMethodHash.parameter = get_method_parameter(pFunctionDef,vMethodHash);
  } else {
    console.log("NEW Method '" + pMethodID + "(" + vParamString + ")' created in UML");
    vMethodHash = {
      "visibility": "public",
      "name": pMethodID,
      "parameter": [],
      "return": " ",
      "comment": "the method performs ...",
      "code": extract_body(pFunctionDef)
    };
    vMethodHash.parameter = get_method_parameter(pFunctionDef,vMethodHash);
  }
  // update the methods in UML model
  return vMethodHash;
}

function get_prototype_methods (pPrototype,pData) {
  var vMethArray = [];
  for (var meth_name in pPrototype) {
    if (pPrototype.hasOwnProperty(meth_name)) {
      console.log("CALL: LoadFile4DOM.prototype." + meth_name + "(" + extract_param(pPrototype[meth_name]) + ")");
      //console.log("FUNCTION: LoadFile4DOM.prototype." + variable + "\nFunction:\n"+vHash[variable]);
      console.log("CODE: LoadFile4DOM.prototype." + meth_name + "\nCode:\n"+extract_body(pPrototype[meth_name]));
      //console.log("PARAM: LoadFile4DOM.prototype." + variable + "\nParam:"+extract_param(vHash[variable]));
      vMethArray.push(get_method(meth_name,pPrototype[meth_name],pData));
    }
  }
  return vMethArray;
}

function get_type_of_attribute(obj) {
  var TYPES = {
    'undefined'        : 'undefined',
    'number'           : 'number',
    'boolean'          : 'boolean',
    'string'           : 'string',
    '[object Function]': 'function',
    '[object RegExp]'  : 'regexp',
    '[object Array]'   : 'array',
    '[object Date]'    : 'date',
    '[object Error]'   : 'error'
  };
  var TOSTRING = Object.prototype.toString;

  return TYPES[typeof obj] || TYPES[TOSTRING.call(obj)] || (obj ? 'object' : 'null');

};

function get_attrib(pAttribID,pType,pDefault,pData) {
  var vAttribHash = null;
  var attrib_i = find_name_index(pAttribID,pData.attributes);
  if (attrib_i >= 0) {
    console.log("Attribute '" + pAttribID + "' Type: '" + pType + "' found");
    // Attrib name found in vUML.data.Attribs
    // update the code
    vAttribHash = cloneJSON(pData.attributes[attrib_i]);
    vAttribHash.init = pDefault;
  } else {
    console.log("NEW Attrib '" + pAttribID + "' Type: '" + pType + "' created in UML");
    vAttribHash = {
      "visibility": "public",
      "name": pAttribID,
      "init": pDefault,
      "comment": "the attribute stores ...",
    };
  }
  // update the methods in UML model
  return vAttribHash;
}


function get_instance_attibutes(pConstructor,pData) {
  var vAttribArray = [];
  var vInstance = new pConstructor();
  for (var variable in vInstance) {
    if (vInstance.hasOwnProperty(variable)) {
      var vType = get_type_of_attribute(vInstance[variable]);
      var vDefault = JSON.stringify(vInstance[variable],null,4);
      console.log("Attribute: 'this." + variable  + " Type: '"+vType+"'  ' Default: " + vDefault);
      vAttribArray.push(get_attrib(variable,vType,vDefault,pData));
    }
  }

  return vAttribArray;
}

vUML.data.methods = get_prototype_methods(vPrototype,vUML.data);

vUML.data.attributes = get_instance_attibutes(vConstructor,vUML.data);


update_uml(vUML,pkg);

var vContent = JSON.stringify(vUML, null, 4);
//console.log("JSCC:\n" + vContent);
fs.writeFile("./jscc/"+vOutputUML, vContent, function(err) {
    if(err) {
        return console.log(err);
    }

    console.log("The file 'jscc/" + vOutputUML + "' was saved!");
});
